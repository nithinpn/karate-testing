Feature: User wants to check Google URL as GET

  Background:
    * url 'https://reqres.in/api/'

  Scenario: Testing valid user

    Given path 'users/2'
    When method GET
    Then status 200
    And print response
    * def actName = response.data.first_name
    * print actName
    * match actName == response.data.first_name

  Scenario: Creating user

    Given path 'users'
    And request
    """
    {
    "name": "morpheus",
    "job": "leader"
    }
    """
    When method POST
    And status 201
    Then match response == "#object"
    And print response


  Scenario: Testing List of users

    Given path 'users/'
    And param page = "2"
    When method GET
    Then status 200
    And print response
   * match response.page == 2


  Scenario: Updating user

    Given path 'users/2'
    And request
    """
    {
    "name": "updated morpheus",
    "job": "leader"
    }
    """
    When method PUT
    And status 200
    Then match response == "#object"
    And print response

  Scenario: Deleting user

    Given path 'users/2'
    When method DELETE
    Then status 204






