package runners;

import com.intuit.karate.KarateOptions;
import com.intuit.karate.junit5.Karate;

@KarateOptions(features = {"classpath:features"})
public class mainRunner {
    @Karate.Test
    Karate TestAll(){
        return Karate.run().relativeTo(getClass());

    }



}
