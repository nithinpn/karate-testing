Feature: User wants to check Google URL as GET

  Background: Set url
    * url 'https://gorest.co.in/public/v2/users'

  Scenario: Testing valid user

    Given path 889
    When method GET
    Then status 200
    And print response
    * def actName = response.name
    * print actName
    * match actName == response.name

  Scenario: Testing invalid user

    Given path 10
    When method GET
    Then status 404
    And print response